import React from 'react';
import product from '../Product';

const Prix = () => {
    return (
        <div>
            Prix : {product.prix} FCFA
        </div>
    );
}

export default Prix;
