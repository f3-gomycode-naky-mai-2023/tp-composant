import React from 'react';
import product from '../Product';

const Image = () => {
    return (
        <div>
            <img src={product.image} alt="image" srcset=""/>
        </div>
    );
}

export default Image;
