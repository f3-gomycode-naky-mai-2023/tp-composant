import logo from './logo.svg';
import './App.css';
import Nom from './components/Nom';
import Description from './components/Description';
import Prix from './components/Prix';
import Image from './components/Image';
import Card from 'react-bootstrap/Card';


function App() {
  return (
    <div>
      
      <Card style={{ width: '18rem' }}>
      <Image></Image>
      <Card.Body>
        <Card.Title><Nom></Nom></Card.Title>
        <Card.Text>
        <Prix></Prix>
        <Description></Description>
        </Card.Text>
      </Card.Body>
    </Card>
     
    </div>
  );
}

export default App;
